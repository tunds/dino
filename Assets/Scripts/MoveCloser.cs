﻿using UnityEngine;
using System.Collections;

public class MoveCloser : MonoBehaviour {


	/**
	 * Reference
	 * https://www.youtube.com/watch?v=CrngGEq2t2w
	 * https://www.youtube.com/watch?v=11vYS0mCmTM
	 * http://stackoverflow.com/questions/35522038/google-cardboard-how-to-detect-focus-on-an-object
	*/
	public Transform target;
	public float speed = 30.0f;

	public Transform cam; // This is the main camera.
	// You can alternately use Camera.main if you've tagged it as MainCamera

	private float timer;

	bool focus; // True if focused

	Collider gazeArea; // Your object's collider

	private Animation animation;

	private float currentPosition;
	private float targetPosition;

	private AudioSource dinoRunningAudio;
	private AudioSource dinoGrowlAudio;

	public void Start () {
		gazeArea = GetComponent<Collider> ();
		animation = GetComponent<Animation> ();

		var sounds = GetComponents<AudioSource>();

		dinoRunningAudio = sounds[1];
		dinoGrowlAudio = sounds[0];

	}

	public void Update () {

		currentPosition = transform.position.z;
		targetPosition = target.position.z;

		RaycastHit hit;

		if (Physics.Raycast (cam.position, cam.forward, out hit, 1000f)) {
			focus = (hit.collider == gazeArea); 
			timer += Time.deltaTime;
			if (timer >= 1.5){

				Debug.Log ("Hit it baby");


				if (currentPosition - targetPosition <= 3) {

					if(!dinoGrowlAudio.isPlaying){

						dinoRunningAudio.Stop ();
						dinoGrowlAudio.Play ();
					}

					if (!animation.IsPlaying ("Allosaurus_Attack02")) {
						animation.Play ("Allosaurus_Attack02");
					}

					 

				} else {

					if(!dinoRunningAudio.isPlaying){

						dinoGrowlAudio.Stop ();
						dinoRunningAudio.Play ();
					}

					if (!animation.IsPlaying("Allosaurus_Run")){
						animation.Play ("Allosaurus_Run");
					}

					transform.position = Vector3.MoveTowards (transform.position, target.position, speed * Time.deltaTime);

					transform.rotation = Quaternion.LookRotation(transform.position - target.position);



				}

			}

		} else {
			timer = 0.0f;
			focus = false;
			if (!animation.IsPlaying ("Allosaurus_IdleAggressive")) {
				animation.Play ("Allosaurus_IdleAggressive");
			}
		}


	}
		
}
