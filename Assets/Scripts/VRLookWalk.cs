﻿using UnityEngine;
using System.Collections;

public class VRLookWalk : MonoBehaviour {

	public Transform vrCamera;

	public float toggleAngle = 10.0f;

	private float speed = 3.0f;

	public bool moveForward;

	private CharacterController myCC;

	private AudioSource walkingAudio;

	// Use this for initialization
	void Start () {

		myCC =  GetComponent<CharacterController> ();
		walkingAudio = GetComponent<AudioSource> ();

	}

	// Update is called once per frame
	void Update () {

		if (vrCamera.eulerAngles.x >= toggleAngle && vrCamera.eulerAngles.x < 60.0f) 
		{

			if (vrCamera.eulerAngles.x >= (toggleAngle * 2) && vrCamera.eulerAngles.x < 60.0f) 
			{
				speed = 6.0f;
			}

			moveForward = true;

		} else {
			moveForward = false;
		}
			
		if (moveForward)
		{
			Vector3 forward = vrCamera.TransformDirection (Vector3.forward);
			myCC.SimpleMove (forward * speed);

			if(!walkingAudio.isPlaying)
			{
				walkingAudio.Play ();
			}

		}	
	}
}
